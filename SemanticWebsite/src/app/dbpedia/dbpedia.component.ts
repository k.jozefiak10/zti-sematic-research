import { Component, OnInit } from '@angular/core';
import { DbpediaService} from './../services/dbpedia.service'
@Component({
  selector: 'mdb-dbpedia',
  templateUrl: './dbpedia.component.html',
  styleUrls: ['./dbpedia.component.scss']
})
export class DbpediaComponent implements OnInit {

  query = 'select distinct ?Concept where {[] a ?Concept} LIMIT 100';
  databaseUri = 'http://dbpedia.org'
  result = '';
  constructor(private dbpediaService: DbpediaService) { }

  ngOnInit() {
  }

  runQuery() {
    this.dbpediaService.runQuery(this.databaseUri, this.query).subscribe((response) => {
      this.result = response.text();
      });
  }

}
