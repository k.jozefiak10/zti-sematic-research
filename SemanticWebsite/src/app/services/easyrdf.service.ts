import { HttpProvider } from './http-provider.service';
import { Injectable } from '@angular/core';
@Injectable()
export class EasyRdfService {

  constructor(private httpProvider: HttpProvider) { }

  public converter(rdfPlain: string) {

    return this.httpProvider
      .route('http://localhost:8080')
      .post('/easy-rdf/rdfxml/svg', rdfPlain )
      .map((r) => r.text());
  }
}
