import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpProvider {

  private apiUrl = 'http://localhost:8080';
  constructor(private http: Http) {
  }

  public route(baseUrl: string) {
    this.apiUrl = baseUrl;
    return this;
  }

  public get(resource: string): Observable<Response> {
    let actionUrl = `${this.apiUrl}/${resource}`;

    return this.http.get(actionUrl);
  }

  public post(resource: string, body: any = {}, additionalHeaders: Headers = new Headers() ): Observable<Response> {
    let actionUrl = `${this.apiUrl}/${resource}`;
    let headers = new Headers(additionalHeaders);
    headers.append('Access-Control-Allow-Headers', this.apiUrl);
    return this.http.post(actionUrl, body, {headers});
  }

  public put(resource: string, body: any = {}): Observable<Response> {
    let actionUrl = `${this.apiUrl}/${resource}`;
    let headers = new Headers();
    headers.append('Access-Control-Allow-Origin', this.apiUrl);

    return this.http.put(actionUrl, body, {headers});
  }
}
