import { Injectable } from '@angular/core';
import { HttpProvider } from './http-provider.service';

@Injectable()
export class DbpediaService {

  constructor(private httpProvider: HttpProvider) { }

  runQuery(databaseUri: string, query: string) {
    let queryString = '/sparql?default-graph-uri=' + databaseUri + '&query=' + query;
    queryString += '&format=text/html&CXML_redir_for_subjs=121&timeout=30000&debug=on&run=Run Query';

    return this.httpProvider
      .route('https://dbpedia.org')
      .get(queryString)
  }
}
