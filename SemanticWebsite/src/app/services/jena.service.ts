import { HttpProvider } from './http-provider.service';
import { Injectable } from '@angular/core';

@Injectable()
export class JenaService {

  constructor(private httpProvider: HttpProvider) { }

  public testMethod(rdf) {
    return this.httpProvider.post('/deserialize/rdf', rdf)
      .map((r) => r.json());
  }

  public getProperites(body) {
    return this.httpProvider.post(`rdf`, body)
      .map((r) => r.json());
  }

}
