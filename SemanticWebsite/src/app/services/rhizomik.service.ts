import { HttpProvider } from './http-provider.service';
import { Injectable } from '@angular/core';
import { Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class RhizomikService {

  constructor(private httpProvider: HttpProvider) { }

  getDiagram(rdfPlainText: string) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let body = new URLSearchParams();
    body.set('rdf', rdfPlainText);
    body.set('roules', 'http://localhost:8080/rdf2svg/rules/showontology.jrule');
    body.set('format', 'RDF/XML');
    body.set('language', 'en');
    body.set('Submit', 'Submit');

    return this.httpProvider
      .route('http://localhost:8080')
      // tslint:disable-next-line:max-line-length
      .post(
        'rdf2svg/render',
        body.toString(),
        headers
      );
  }
}
