import { Component, OnInit } from '@angular/core';
import { JenaService } from '../services/jena.service';

@Component({
  selector: 'mdb-jena',
  templateUrl: './jena.component.html',
  styleUrls: ['./jena.component.css']
})
export class JenaComponent implements OnInit {
  public rdfPlain = `<?xml version="1.0" encoding='utf-8'?>`;
  public formattedRdf = '';
  public properties = [];
  public property = '';
  public schema = '';


  constructor(private jenaService: JenaService) { }

  ngOnInit() {
  }

  public load() {
    this.jenaService.testMethod(this.rdfPlain)
      .subscribe((response) => {
        this.formattedRdf = response;
      });
  }

  public getProperites() {
    this.jenaService.getProperites(`${this.schema}#${this.property}`)
      .subscribe((response) => {
        this.properties = response;
      });
  }
}
