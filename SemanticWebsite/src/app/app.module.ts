import { RhizomikService } from './services/rhizomik.service';
import { DbpediaService } from './services/dbpedia.service';
import { EasyRdfService } from './services/easyrdf.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MDBBootstrapModule } from './typescripts/free';
import { AgmCoreModule } from '@agm/core';
import { AppComponent } from './app.component';
import { JenaService } from './services/jena.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpProvider } from './services/http-provider.service';
import { JenaComponent } from './jena/jena.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TodoComponent } from './todo/todo.component';
import { RouterModule } from '@angular/router';
import { EasyrdfComponent } from './easyrdf/easyrdf.component';
import { DbpediaComponent } from './dbpedia/dbpedia.component';
import { RhizomikComponent } from './rhizomik/rhizomik.component';

@NgModule({
  declarations: [
    AppComponent,
    JenaComponent,
    NavbarComponent,
    TodoComponent,
    EasyrdfComponent,
    DbpediaComponent,
    RhizomikComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MDBBootstrapModule.forRoot(),
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    RouterModule.forRoot([
            { path: '', redirectTo: 'jena', pathMatch: 'full' },
            { path: 'jena', component: JenaComponent },
            { path: 'todo', component: TodoComponent },
            { path: 'easyrdf', component: EasyrdfComponent},
            { path: 'dbpedia', component: DbpediaComponent},
            { path: 'rhizomik', component: RhizomikComponent},
            { path: '**', redirectTo: 'jena' }
        ])
  ],
  providers: [
    HttpProvider,
    JenaService,
    EasyRdfService,
    DbpediaService,
    RhizomikService,
  ],
  bootstrap: [AppComponent],
  schemas:      [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
