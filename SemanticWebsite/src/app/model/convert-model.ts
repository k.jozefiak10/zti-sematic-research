export class ConvertModel {
    constructor(public data: string, public ins: string, public out: string, public uri: string) {}
    public toJson() {
        return {data: this.data, in: this.ins, out: this.out, uri: this.uri};
    }
}
