import { Component, OnInit } from '@angular/core';
import { EasyRdfService } from '../services/easyrdf.service';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'mdb-easy-rdf',
  templateUrl: './easyrdf.component.html',
  styleUrls: ['./easyrdf.component.scss']
})
export class EasyrdfComponent implements OnInit {

  rdfPlain = `<?xml version="1.0"?>

  <rdf:RDF xml:lang="en"
          xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
  
  <rdf:Description ID="biologicalParent">
    <rdf:type resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
  </rdf:Description>
  
  <rdf:Description ID="biologicalFather">
    <rdf:type resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
    <rdfs:subPropertyOf rdf:resource="#biologicalParent"/>
  </rdf:Description>
  
  </rdf:RDF>`;
  
  htmlInject = '';
  myImage: SafeHtml;

  constructor(private easyrdfService: EasyRdfService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  load() {
    this.easyrdfService
    .converter(this.rdfPlain)
    .subscribe((response) => {
      this.myImage = this.sanitizer.bypassSecurityTrustHtml(response);
    });
  }

}
