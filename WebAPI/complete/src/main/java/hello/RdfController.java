package hello;

import org.apache.jena.rdf.model.*;
import org.apache.jena.util.FileManager;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class RdfController {

    private static final RdfInterpreter rdfInterpreter = new RdfInterpreter();

    @RequestMapping(value = "/deserialize/rdf", method = RequestMethod.POST)
    public RdfSummaryModel deserializeRdf(@RequestBody String plainRdf) throws JSONException {
        RdfModel.Model = getModelFromPlainText(plainRdf);
        return rdfInterpreter.run(RdfModel.Model);
    }

    @RequestMapping(value = "/rdf", method = RequestMethod.POST)
    public ArrayList<String> rdf(@RequestBody String uri) throws JSONException {
        return rdfInterpreter.getProperties(RdfModel.Model, uri);
    }

    private Model getModelFromPlainText(String plainText){
        Model model = ModelFactory.createDefaultModel();
        ByteArrayInputStream ba = new ByteArrayInputStream(plainText.getBytes());
        model.read(ba, null);
        return  model;
    }

    static class RdfModel  {
        public static Model Model;
    }
}
