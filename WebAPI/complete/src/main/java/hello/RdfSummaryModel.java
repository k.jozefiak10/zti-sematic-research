package hello;

import java.util.ArrayList;

public class RdfSummaryModel {
    private ArrayList<DescriptionNode> nodes;
    private ArrayList<String> namespaces;

    public RdfSummaryModel(ArrayList<DescriptionNode> nodes, ArrayList<String> namespaces){
        this.namespaces = namespaces;
        this.nodes = nodes;
    }

    public ArrayList<DescriptionNode> getNodes() {
        return nodes;
    }

    public ArrayList<String> getNamespaces() {
        return namespaces;
    }
}
