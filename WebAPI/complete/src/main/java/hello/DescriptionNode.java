package hello;

import java.util.ArrayList;

public class DescriptionNode {
    private String localName;
    private String nameSpace;
    private String uri;
    private ArrayList<KeyValueModel> subNodes;

    public DescriptionNode(String localName, String nameSpace, String uri, ArrayList<KeyValueModel> subNodes){
        this.localName = localName;
        this.nameSpace = nameSpace;
        this.uri = uri;
        this.subNodes= subNodes;
    }

    public String getLocalName() {
        return localName;
    }

    public String getUri() {
        return uri;
    }

    public ArrayList<KeyValueModel> getSubNodes() {
        return subNodes;
    }

    public String getNameSpace() {
        return nameSpace;
    }
}
