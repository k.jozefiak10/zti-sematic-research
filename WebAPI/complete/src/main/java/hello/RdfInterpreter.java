package hello;

import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.VCARD;

import java.util.ArrayList;

public class RdfInterpreter {
    public ArrayList<String> getProperties(Model model, String uri) {
        Property prop = ResourceFactory.createProperty(uri);
        ResIterator iter = model.listSubjectsWithProperty(prop);
        ArrayList<String> result = new ArrayList<>();
        while(iter.hasNext()){
            try {
                Resource res = iter.next();
                String st = getResourceProperty(res, prop);
                result.add(st);
            } catch (Exception ex){}
        }
        return  result;
    }

    private String getResourceProperty(Resource res, Property prop){
            try {
                return res
                    .getPropertyResourceValue(prop)
                    .getLocalName();
            }
            catch(Exception ex){
               return  getProperty(res, prop);
            }
    }

    private String getProperty(Resource res, Property prop){
        return res
            .getProperty(prop)
            .getString();
    }


    public RdfSummaryModel run(Model model){
        ArrayList<String> namespaces = getNamespaces(model);
        ArrayList<DescriptionNode> nodes = getNodes(model);
        return new RdfSummaryModel(nodes, namespaces);
    }

    private ArrayList<String> getNamespaces(Model model){
        ArrayList<String> namespaces = new ArrayList<>();
        NsIterator ni = model.listNameSpaces();
        while(ni.hasNext()) {
            namespaces.add(ni.nextNs());
        }
        return  namespaces;
    }

    private ArrayList<DescriptionNode> getNodes(Model model){
        ArrayList<DescriptionNode> nodes = new ArrayList<DescriptionNode>();
        ResIterator it = model.listSubjects();
        while(it.hasNext()) {
            Resource r = it.nextResource();
            nodes.add(getDescriptionNode(r));
        }
        return nodes;
    }

    private DescriptionNode getDescriptionNode(Resource r){
        String localName = r.getLocalName();
        String uri = r.getURI();
        String nameSpace = r.getNameSpace();
        ArrayList<KeyValueModel> values = getKeyValueCollection(r.listProperties());
        return  new DescriptionNode(localName, nameSpace, uri, values);
    }

    private ArrayList<KeyValueModel> getKeyValueCollection(StmtIterator it){
        ArrayList<KeyValueModel> result = new ArrayList<KeyValueModel>();
        while(it.hasNext()){
            Statement st=it.nextStatement();
            result.add(getKeyValueModel(st));
        }
        return  result;
    }

    private KeyValueModel getKeyValueModel(Statement st){

        String prop=st.getPredicate().getLocalName();
        String value =st.getObject().toString();
        return new KeyValueModel(prop, value);
    }
}
