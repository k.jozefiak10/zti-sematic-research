package hello;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RestController
public class EasyRdfController {

    @RequestMapping(value = "/easy-rdf/{in}/{out}", method = RequestMethod.POST)
    public String deserializeRdf(@PathVariable String in, @PathVariable String out, @RequestBody String plainRdf) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://www.easyrdf.org/converter");
        String body = "data=" + URLEncoder.encode(plainRdf, "UTF-8") + "&in=" + in + "&out=" + out + "&uri=" + URLEncoder.encode("http://njh.me/", "UTF-8") ;
        HttpEntity  entity = new StringEntity(body, ContentType.APPLICATION_FORM_URLENCODED);
        httpPost.setEntity(entity);
        CloseableHttpResponse response = client.execute(httpPost);
        HttpEntity entityResponse = response.getEntity();
        String responseString = EntityUtils.toString(entityResponse, "UTF-8");
        client.close();
        Document dc = Jsoup.parse(responseString);
        String text =  dc.select("pre#result").first().select("code").text();
        int i = text.indexOf("<svg");
        return text.substring(i);
    }
}
